<!DOCTYPE html>
<?php
include_once 'config/dbconfig.php';

if(isset($_REQUEST['act'])==1)
{
	$dailytime= $_REQUEST['dailytime'];
	$dbpath = $_REQUEST['dbpath'];
	//echo $dbpath."<BR>";
	$dbpath = str_replace("\\", "|", $dbpath);
	//echo $dbpath."<BR>";
	//更新資料
	$upd_times = "update hams_config set hams_configvar=$dailytime where hams_configname='hamsdailystart'";
	//echo $upd_times;
	//$upd_path = "update hams_config set hams_configvar=replace('".$dbpath."','\','|') where hams_configname='hamsmdbpath'";
	$upd_path = "update hams_config set hams_configvar='$dbpath' where hams_configname='hamsmdbpath'";
	//echo $upd_path;
	mysql_query($upd_times);
	mysql_query($upd_path);
	
}
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>設定頁面</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="nav nav-tabs">
				<li>
					<a href="index.php">main</a>
				</li>
				<li class="active">
					<a href="#">設定頁</a>
				</li>
				<li>
					<a href="classtype.php">班表設定</a>
				</li>
				<li>
					<a href="manclass.php">人員班表設定</a>
				</li>
				<li>
					<a href="deptlist.php">部門別管理</a>
				</li>
				<li>
					<a href="hams_statistics.php">津貼費用計算</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<form name='getdata' method="post" action='get_mdb_data.php'>
 			 我要抓
			 <select name="syear">
			 	<?php
			 		//今年
			 		$thisyear = date('Y');
					//echo $thisyear;
			 		for($syear=$thisyear;$syear>2009;$syear--)
					{
						echo "<option value='$syear'>$syear</option>";
					}
			 	?>
			 </select>年
			 <select name="smonth">
			 	<?php
			 		for($smon=1;$smon<13;$smon++)
					{
						if(strlen($smon)<2)
						{
							$smon = '0'.$smon;
						}
						echo "<option value='$smon'>$smon</option>";
					}
			 	?>
			 </select>月的資料
			 
			 <button type="submit" class="btn btn-info">取得資料</button>
			 </form>
			 <br>
			 <form name='getdata' method="post" action='del_mdb_data.php'>
			 	<input type='hidden' name='bymonth' value='1'>
 			 我要清除
			 <select name="syear">
			 	<?php
					//echo $thisyear;
			 		for($syear=$thisyear;$syear>2009;$syear--)
					{
						echo "<option value='$syear'>$syear</option>";
					}
			 	?>
			 </select>年
			 <select name="smonth">
			 	<?php
			 		for($smon=1;$smon<13;$smon++)
					{
						if(strlen($smon)<2)
						{
							$smon = '0'.$smon;
						}
						echo "<option value='$smon'>$smon</option>";
					}
			 	?>
			 </select>月的資料
			 <button type="submit" class="btn btn-info">清除資料</button>
			 </form>
			 <form name='getdata' method="post" action='del_mdb_data.php'>
			 	<input type='hidden' name='clsall' value='1'>
			  <button type="submit" class="btn btn-info">清除全部資料</button>
			 </form>
			 <br>
			 <form name="config" method="post" action="config.php">
			 	<input type="hidden" name="act" value="1">
				 <p>
				 出缺勤DB位置在：<input type='text' name='dbpath' value='<?php echo str_replace('|', "\\", $dbpath)?>' size='50'><br>
				 上班時間往前抓：<input type='text' name='dailytime' value='<?php echo $dailytime?>' size='10'>小時
				 </p>
				 <input type="submit" name='btn_sub' value="修改送出">
			 </form>
		</div>
	</div>
</div>
</body>
</html>
