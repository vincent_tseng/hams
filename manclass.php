<!DOCTYPE html>
<?php
include_once 'config/dbconfig.php';

if(isset($_REQUEST['chk_all']))
{
	$all_checked='checked';
}
else {
	$all_checked='';
}
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>主頁面</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	
	<script language="JavaScript">
		function modify(cid)
		{
			t_url ='manclassmod.php?cid='+cid
			location.href=t_url;
		}
		function del_rec(idx,dis)
		{
			var act = 'man';
			$.ajax({
				url:'ajax_mod.php',
				typr:'POST',
				data:'&act='+act+'&idx='+idx+'&dis='+dis,
				success:function(response)
				{
					if(response==0)
					{
						alert('更新失敗');
					}
					else
					{
						alert('更新成功');	
						if(dis==0)
						{
							window.location.href='manclass.php?chk_all=1'
						}
						else
						{
							window.location.reload();
						}
						
					}
				}
			})
			
		}
		
		function del_fer(idx)
		{
			var act = 'man_ever';
			if(confirm('確定要永久刪除嗎？'))
			{
				$.ajax({
					url:'ajax_mod.php',
					typr:'POST',
					data:'&act='+act+'&idx='+idx,
					success:function(response)
					{
						if(response==0)
						{
							alert('更新失敗');
						}
						else
						{
							alert('更新成功');	
							window.location.reload();
							
						}
					}
				})
			}
		}
		
		function del_all(tab)
		{
			var act = 'del_all';
			if(confirm('確定要全部刪除嗎？'))
			{
				$.ajax({
					url:'ajax_mod.php',
					typr:'POST',
					data:'act='+act+'&tab='+tab,
					success:function(response)
					{
						if(response==0)
						{
							alert('更新失敗');
						}
						else
						{
							alert('更新成功');	
							window.location.reload();
							
						}
					}
				})
			}
		}
		
		function rep_all(tab)
		{
			var act = 'rep_all';
			if(confirm('確定要全部復原嗎？'))
			{
				$.ajax({
					url:'ajax_mod.php',
					typr:'POST',
					data:'act='+act+'&tab='+tab,
					success:function(response)
					{
						if(response==0)
						{
							alert('更新失敗');
						}
						else
						{
							alert('更新成功');	
							window.location.reload();
							
						}
					}
				})
			}
		}
	</script>
</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="nav nav-tabs">
				<li>
					<a href="index.php">main</a>
				</li>
				<li>
					<a href="config.php">設定頁</a>
				</li>
				<li>
					<a href="classtype.php">班表設定</a>
				</li>
				<li class="active">
					<a href="#">人員班表設定</a>
				</li>
				<li>
					<a href="deptlist.php">部門別管理</a>
				</li>
				<li>
					<a href="hams_statistics.php">津貼費用計算</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<button type="button" class="btn btn-info" onclick="location.href='manclassnew.php'">新增人員</button>
			<form name='manclass' method="Post" action='manclass.php'>
			<input type='hidden' name='act' value='1'>
			<div>
				請輸入卡號：<input type="text" name='eventCard'><br>
				或請選擇部門別
				<select name='dept'>
					<option value="0">請選擇</option>
				<?php
					$dept_sql ="select * from hams_dept where disabled=0";
					$result = mysql_query($dept_sql);
					$dnums = mysql_num_rows($result);
					for($dd=0;$dd<$dnums;$dd++)
					{
						mysql_data_seek($result, $dd);
						$ddet = mysql_fetch_array($result);
						if(isset($_REQUEST['act'])==1)
						{
							if($ddet['idx']==$_REQUEST['dept'])
							{
								echo "<option value='".$ddet['idx']."' selected>".$ddet['hams_deptName']."</option>";
							}
							else
							{
								echo "<option value='".$ddet['idx']."'>".$ddet['hams_deptName']."</option>";	
							}
						}
						else
						{
							echo "<option value='".$ddet['idx']."'>".$ddet['hams_deptName']."</option>";
						}
						 
					}

				?>
				</select><br>
				或請選擇班表別
				<select name='classtype'>
				    <option value="0">請選擇</option>
                <?php
                    $dept_sql ="select * from hams_classtype where disabled=0";
                    $result = mysql_query($dept_sql);
                    $dnums = mysql_num_rows($result);
                    for($dd=0;$dd<$dnums;$dd++)
                    {
                        mysql_data_seek($result, $dd);
                        $ddet = mysql_fetch_array($result);
                        if(isset($_REQUEST['act'])==1)
                        {
                            if($ddet['idx']==$_REQUEST['classtype'])
                            {
                                echo "<option value='".$ddet['idx']."' selected>".$ddet['Hams_classname']."</option>";
                            }
                            else
                            {
                                echo "<option value='".$ddet['idx']."'>".$ddet['Hams_classname']."</option>";    
                            }
                        }
                        else
                        {
                            echo "<option value='".$ddet['idx']."'>".$ddet['Hams_classname']."</option>";
                        }
                         
                    }

                ?>
				</select>
				<br><input type='checkbox' name='chk_all' value='1' <?php echo $all_checked?>>顯示全部<br>
				<input type='submit' name='btn_sub' value='送出'>
			</div>
			</form>
			<div align="right">
				<button type="button" class="btn btn-info" onclick="del_all('Hams_persion')">全部刪除</button>
				<button type="button" class="btn btn-info" onclick="rep_all('Hams_persion')">全部復原</button>
			</div>
		</div>
		
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table">
				<thead>
					<tr>
						<th>
							#
						</th>
						<th>
							人員
						</th>
						<th>
							人員卡號
						</th>
						<th>
							部門別
						</th>
						<th>
							班別
						</th>
						<th>
							上下班時間
						</th>
						<th>
							
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$chk_dept=0;
					$chk_personName = 0;
					if(isset($_REQUEST['chk_all']))
					{
						$showall='';
						$showall_1='';
					}
					else {
						$showall=' where a.disabled=0';	 
						$showall_1 = 'and a.disabled=0';
					}
						if(isset($_REQUEST['act']))
						{					
							$sql = "select a.idx,eventCard,hams_deptName,persionName,Hams_classname,hams_classstarttime,hams_classendtime,a.disabled from hams_persion a join hams_classtype b on a.classtype=b.idx join hams_dept c on a.dept=c.idx";
							if($_REQUEST['eventCard']=="" && $_REQUEST['dept']=="0" && $_REQUEST['classtype']=='0')
							{
									$sql = $sql .$showall;
							}
							else {
								$sql = $sql ." where (c.idx='".$_REQUEST['dept']."' or eventCard='".$_REQUEST['eventCard']."' or b.idx='".$_REQUEST['classtype']."') ".$showall_1;
							}
						}
						else
						{
							$sql = "select a.idx,eventCard,hams_deptName,persionName,Hams_classname,hams_classstarttime,hams_classendtime,a.disabled from hams_persion a join hams_classtype b on a.classtype=b.idx join hams_dept c on a.dept=c.idx".$showall;	
						}
						//echo $showall."<BR>";
						//echo $sql;
						$ctrs = mysql_query($sql);
						$ctnums = mysql_num_rows($ctrs);
						for($i=0;$i<$ctnums;$i++)
						{
							mysql_data_seek($ctrs, $i);
							$detail = mysql_fetch_array($ctrs);
							$cid = $detail['idx'];
							
							if($detail['disabled']==0)
							{
								$tr_color="";
								$str_del = "<button type='button' class='btn btn-info' onclick=del_rec('$cid','1')>刪除</button>";
							}
							else
							{
								$tr_color ="class='danger'";
								$str_del = "<button type='button' class='btn btn-info' onclick=del_rec('$cid','0')>復原</button>";
								$str_del = $str_del ."<button type='button' class='btn btn-info' onclick=del_fer('$cid')>永久刪除</button>";
							}
							
					?>
					<tr <?php echo $tr_color;?>>
						<td>
							<?php echo $cid;?>
						</td>
						<td>
							<?php echo $detail['persionName']?>
						</td>
						<td>
							<?php echo $detail['eventCard']?>
						</td>
						<td>
							<?php echo $detail['hams_deptName']?>
						</td>
						<td>
							<?php echo $detail['Hams_classname']?>
						</td>
						<td>
							<?php echo $detail['hams_classstarttime']?> - <?php echo $detail['hams_classendtime']?>
						</td>
						<td>
							<button type="button" class="btn btn-info" onclick="modify(<?php echo $cid;?>)">修改</button>
							<?php echo $str_del;?>
						</td>
					</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>
