<!DOCTYPE html>
<?php
include_once 'config/dbconfig.php';
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>主頁面</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript" src="js/jquery.ui.datepicker-zh-TW.js"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	
	<script language="JavaScript">
		$(function() {
		    $( "#log_date_start" ).datepicker({ 
		    	dateFormat: "yy/mm/dd" 
		    });
		    $( "#log_date_end" ).datepicker({ 
		    	dateFormat: "yy/mm/dd" 
		    });
		  });
	</script>
</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="nav nav-tabs">
				<li>
					<a href="index.php">main</a>
				</li>
				<li>
					<a href="config.php">設定頁</a>
				</li>
				<li>
					<a href="classtype.php">班表設定</a>
				</li>
				<li>
					<a href="manclass.php">人員班表設定</a>
				</li>
				<li>
					<a href="deptlist.php">部門別管理</a>
				</li>
				<li  class="active">
					<a href="#">津貼費用計算</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<form name='index' id='index' method="post" action="hams_statistics.php">
				<input type='hidden' name='act' value="1">
				請選擇：<br /><br />
				請輸入人名或編號：<input type='text' name='pid'><br /><br />
				或選擇班別<select name='classtype' id='classtype'>
                <option value="0">請選擇</option>
                <?php
                    
                    $dsql ="select * from hams_classtype where disabled=0";
                    $dresult = mysql_query($dsql);
                    $dnums = mysql_num_rows($dresult);
                    for($dd=0;$dd<$dnums;$dd++)
                    {
                        mysql_data_seek($dresult, $dd);
                        $ddet = mysql_fetch_array($dresult);
                        if(isset($_REQUEST['classtype'])==1)
                        {
                            if($ddet['idx']==$_REQUEST['classtype'])
                            {
                                echo "<option value='".$ddet['idx']."' selected>".$ddet['Hams_classname']."</option>";
                            }
                            else 
                            {
                                echo "<option value='".$ddet['idx']."'>".$ddet['Hams_classname']."</option>";    
                            }
                            
                        }
                        else 
                        {
                            echo "<option value='".$ddet['idx']."'>".$ddet['Hams_classname']."</option>";    
                        }   
                    }
                ?>
                </select>或<br /><br>
				部門<select name='dept' id='dept'>
				<option value="0">請選擇</option>
				<?php
					
					$dsql ="select * from hams_dept where disabled=0";
					$dresult = mysql_query($dsql);
					$dnums = mysql_num_rows($dresult);
					for($dd=0;$dd<$dnums;$dd++)
					{
						mysql_data_seek($dresult, $dd);
						$ddet = mysql_fetch_array($dresult);
						if(isset($_REQUEST['dept'])==1)
						{
							if($ddet['idx']==$_REQUEST['dept'])
							{
								echo "<option value='".$ddet['idx']."' selected>".$ddet['hams_deptName']."</option>";
							}
							else 
							{
								echo "<option value='".$ddet['idx']."'>".$ddet['hams_deptName']."</option>";	
							}
							
						}
						else 
						{
							echo "<option value='".$ddet['idx']."'>".$ddet['hams_deptName']."</option>";	
						}	
					}
				?>
				</select>
				<br><br />
				<input type='checkbox' name='all_per' value='Y'>顯示全體津貼
				<br /><br />
					開始:<input type="text" name="log_date_start" id="log_date_start" readonly size="20">
					結束:<input type="text" name="log_date_end" id="log_date_end" size="20" readonly>
			<input type='submit' name='btn_sub' value='送出'/>
			</form>
		</div>
	</div>
	<?php if(isset($_REQUEST['act'])){
	    //日期區間
                        $start_date = $_REQUEST['log_date_start'];
                        $end_date = $_REQUEST['log_date_end'];
                        
                        if(isset($_REQUEST['all_per']))
                        {
                            $all_per = $_REQUEST['all_per'];
                        }else{
                            $all_per='N';
                        }
                        $pid = $_REQUEST['pid'];
                        $dept = $_REQUEST['dept'];
                        $classtype = $_REQUEST['classtype'];
	    ?>
	<div>時間從<?php echo $_REQUEST['log_date_start']?>開始到<?php echo $_REQUEST['log_date_end']?></div>
	<div>
	    <form name='tranexcel' action='trans_excel_sta.php'>
	        <input type='hidden' name='pid' value='<?php echo $pid;?>'>
	        <input type='hidden' name='dept' value='<?php echo $dept;?>'>
	        <input type='hidden' name='classtype' value='<?php echo $classtype;?>'>
	        <input type='hidden' name='all_per' value='<?php echo $all_per;?>'>
	        <input type='hidden' name='start_date' value='<?php echo $start_date;?>'>
	        <input type='hidden' name='end_date' value='<?php echo $end_date;?>'>
	    <button type="submit" class="btn btn-info">轉成Excel</button>
	    </form>
	    </div>
	<?php }?>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table">
				<thead>
					<tr>
						<th>
							#
						</th>
						<th>
							姓名
						</th>
						<th>
							班別
						</th>
						<th>
							部門
						</th>
						<th>
							津貼加總
						</th>
						<th>
							誤餐加總
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(isset($_REQUEST['act']))
					{
						//$pid= $_REQUEST['Pid'];
						//$month = $_REQUEST['month'];
						$arr_sql = array();
						$sql = "SELECT persionName,Hams_classname,hams_deptName,sum(Hams_dinner) as Hams_dinner ,sum(Hams_allowance) as Hams_allowance,hams_allowancepay FROM hams_time a join hams_persion b on a.Hams_CardNo = b.eventCard join hams_classtype c on b.classtype=c.idx join hams_dept d on b.dept=d.idx WHERE b.disabled=0 ";
						if($all_per!='Y')
                        {
                            
                            if($dept) //部門
                            {
                                $arr_sql[] =" b.dept = '$dept' ";
                            }
                            
                            if($pid) //人員
                            {
                                $arr_sql[] =" (a.Hams_CardNo = '$pid' or persionName='$pid')";
                            }
                            if($classtype) //部門
                            {
                                $arr_sql[] =" b.classtype = '$classtype'";
                            }
                            if(count($arr_sql)>0)
                            {
                                $sql.=" and (";
                                $sql.= implode(' or ', $arr_sql);
                                $sql.=")"; 
                            }
                            
                        }
						
                        
						if($start_date && $end_date) //時間
						{
							$sql .=" and (Hams_date between '$start_date' and '$end_date') ";
						}
						$sql .= " group by Hams_CardNO";
						//echo $sql;

						$result= mysql_query($sql);
						$nums = mysql_num_rows($result);
						for($ii=0;$ii<$nums;$ii++)
						{
							mysql_data_seek($result, $ii);
							$detail = mysql_fetch_array($result);
							if($detail['hams_allowancepay']==0)
							{
								$day_count=0;
							}else{
								$day_count = $detail['Hams_allowance']/$detail['hams_allowancepay']	;
							}
							

					?>
					<tr>
						<td>
							<?php echo $ii?>
						</td>
						<td>
							<?php echo $detail['persionName'];?>
						</td>
						<td>
							<?php echo $detail['Hams_classname'];?>
						</td>
						<td>
							<?php echo $detail['hams_deptName'];?>
						</td>
						<td>
							<?php echo $detail['Hams_allowance'];?>(<?php echo $day_count;?>)
						</td>
						<td>
							<?php echo $detail['Hams_dinner'];?>(<?php echo ($detail['Hams_dinner']/90);?>)
						</td>
					</tr>
					<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>
