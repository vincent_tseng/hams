<!DOCTYPE html>
<?php
include_once 'config/dbconfig.php';
if(isset($_REQUEST['act'])==1)
{
	//組合時間字串
	
	$stime = $_REQUEST['shour'].":".$_REQUEST['smin'];
	$etime = $_REQUEST['ehour'].":".$_REQUEST['emin'];
	$classname= $_REQUEST['classtype'];
	$chkclasstype=isset($_REQUEST['chk_classtype']);
	$chkallowance=isset($_REQUEST['chk_allowance']);
	$allowancepay = $_REQUEST['allowancepay'];
	$chktwodays=isset($_REQUEST['chk_twodays']);
	$chkovertime=isset($_REQUEST['chk_overtime']);
	$chkcar=isset($_REQUEST['chk_car']);
	$chkovertimeshow=isset($_REQUEST['chk_overtimeshow']);
	
	
	
	$sql = "insert into hams_classtype(Hams_classname,Hams_classstarttime,Hams_classendtime,Hams_classtype,hams_chkallowance,hams_allowancepay,hams_twodays,hams_chkovertime,hams_car,hams_overtimeshow)";
	$sql = $sql."VALUES('$classname','$stime','$etime','$chkclasstype','$chkallowance','$allowancepay','$chktwodays','$chkovertime','$chkcar','$chkovertimeshow')";
	//echo $sql;
	mysql_query($sql);
	
}
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>主頁面</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#">新增班別</a>
				</li>
				<li>
					<a href="classtype.php">回到班表list</a>
				</li>
			</ul>
			<form role="form" name='classtype' action="classtypenew.php" method="post">
				<input type='hidden' name='act' value="1">
				<div class="form-group">
					 <label for="exampleInputEmail1">班表別</label><input type="text" class="form-control" id="classtype" name='classtype' />
				</div>
				<div class="form-group">
					 <label for="exampleInputPassword1">上班時間</label>
					 <select name="shour">
					 <?php
					 	for($i=0;$i<24;$i++)
						{
							if($i<10)
							{
								$kk ='0'.$i;
								echo "<option value='$kk'>$kk</option>";
							}
							else {
								echo "<option value='$i'>$i</option>";
							}
							
						}		 
					 ?>
					 </select>時
					 <select name="smin">
					 <?php
					 	for($si=0;$si<60;$si++)
						{
							if($si<10)
							{
								$kk ='0'.$si;
								echo "<option value='$kk'>$kk</option>";
							}
							else {
								echo "<option value='$si'>$si</option>";
							}
							
						}		 
					 ?>
					 </select>分
				</div>
				<div class="form-group">
					 <label for="exampleInputPassword1">下班時間</label>
					 <select name="ehour">
					 <?php
					 	for($i=0;$i<24;$i++)
						{
							if($i<10)
							{
								$kk ='0'.$i;
								echo "<option value='$kk'>$kk</option>";
							}
							else {
								echo "<option value='$i'>$i</option>";
							}
							
						}		 
					 ?>
					 </select>時
					 <select name="emin">
					 <?php
					 	for($si=0;$si<60;$si++)
						{
							if($si<10)
							{
								$kk ='0'.$si;
								echo "<option value='$kk'>$kk</option>";
							}
							else {
								echo "<option value='$si'>$si</option>";
							}
							
						}		 
					 ?>
					 </select>分
				</div>
				<div class="checkbox">
					 <label><input type="checkbox" name='chk_classtype' value='1' />此班別會跨日</label>
				</div>
				<div class="checkbox3">
					<label><input type="checkbox" name='chk_twodays' value='1' />此班別有週休二日</label>
				</div>
				<div class="checkbox4">
					<label><input type="checkbox" name='chk_overtime' value='1' />此班別有加班補休</label>
				</div>
				<div class="checkbox4">
					<label><input type="checkbox" name='chk_overtimeshow' value='1' />此班別有加班判斷</label>
				</div>
				<div class="checkbox4">
					<label><input type="checkbox" name='chk_car' value='1' />此班別有車資判斷</label>
				</div>
				<div class="checkbox">
					 <label><input type="checkbox" name='chk_allowance' value='1' />此班別有輪值津貼</label>
				</div>
				<div class="checkbox">
					 <label>輪值津貼:<input type="text" name='allowancepay' value='' /></label>
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
	</div>
</div>
</body>
</html>
