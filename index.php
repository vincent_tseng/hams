<!DOCTYPE html>
<?php
include_once 'config/dbconfig.php';
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>主頁面</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript" src="js/jquery.ui.datepicker-zh-TW.js"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	
	<script language="JavaScript">
		function modify(pid,idx,month)
		{
			t_url = 'mansigndaymod.php?idx='+idx+'&pid='+pid+'&month='+month;
			location.href=t_url;
		}
		function cong_hoilday(pid,month)
		{
			t_url = 'manhoilday.php?pid='+pid+'&month='+month;
			location.href=t_url;
		}
		function chan_excel(pid,month)
		{
			t_url = 'trans_excel.php?pid='+pid+'&month='+month;
			location.href=t_url;
		}
		$(document).ready(function(){
			$( "#log_date_start" ).datepicker({ 
                dateFormat: "yy/mm/dd" 
            });
            $( "#log_date_end" ).datepicker({ 
                dateFormat: "yy/mm/dd" 
            });
		})
	</script>
</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#">main</a>
				</li>
				<li>
					<a href="config.php">設定頁</a>
				</li>
				<li>
					<a href="classtype.php">班表設定</a>
				</li>
				<li>
					<a href="manclass.php">人員班表設定</a>
				</li>
				<li>
					<a href="deptlist.php">部門別管理</a>
				</li>
				<li>
					<a href="hams_statistics.php">津貼費用計算</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<form name='index' id='index' method="post" action="index.php">
				<input type='hidden' name='act' value="1">
				請輸入人名或編號：<input type='text' name='pid'><br /><br />
                或選擇班別<select name='classtype' id='classtype'>
                <option value="0">請選擇</option>
                <?php
                    
                    $dsql ="select * from hams_classtype where disabled=0";
                    $dresult = mysql_query($dsql);
                    $dnums = mysql_num_rows($dresult);
                    for($dd=0;$dd<$dnums;$dd++)
                    {
                        mysql_data_seek($dresult, $dd);
                        $ddet = mysql_fetch_array($dresult);
                        if(isset($_REQUEST['classtype'])==1)
                        {
                            if($ddet['idx']==$_REQUEST['classtype'])
                            {
                                echo "<option value='".$ddet['idx']."' selected>".$ddet['Hams_classname']."</option>";
                            }
                            else 
                            {
                                echo "<option value='".$ddet['idx']."'>".$ddet['Hams_classname']."</option>";    
                            }
                            
                        }
                        else 
                        {
                            echo "<option value='".$ddet['idx']."'>".$ddet['Hams_classname']."</option>";    
                        }   
                    }
                ?>
                </select>或<br /><br>
			    選部門<select name='dept' id='dept'>
				<option value="0">請選擇</option>
				<?php
					
					$dsql ="select * from hams_dept where disabled=0";
					$dresult = mysql_query($dsql);
					$dnums = mysql_num_rows($dresult);
					for($dd=0;$dd<$dnums;$dd++)
					{
						mysql_data_seek($dresult, $dd);
						$ddet = mysql_fetch_array($dresult);
						if(isset($_REQUEST['dept'])==1)
						{
							if($ddet['idx']==$_REQUEST['dept'])
							{
								echo "<option value='".$ddet['idx']."' selected>".$ddet['hams_deptName']."</option>";
							}
							else 
							{
								echo "<option value='".$ddet['idx']."'>".$ddet['hams_deptName']."</option>";	
							}
							
						}
						else 
						{
							echo "<option value='".$ddet['idx']."'>".$ddet['hams_deptName']."</option>";	
						}	
					}
				?>
				</select>
				<br /><br />
			年/月份：
			<select name='month'>
			    <option value='0'>請選擇</option>
				<?php
					$sql2=  "select substring(Hams_date,1,7) as str_date from hams_time group by substring(Hams_date,1,7)";
					$res2 = mysql_query($sql2);
					$nums2 = mysql_num_rows($res2);
					if($nums2<1)
					{
						$btn_sand='disabled';
					}
					else {
						$btn_sand='';
					}
					for($ii=0;$ii<$nums2;$ii++)
					{
						mysql_data_seek($res2, $ii);
						$dett= mysql_fetch_array($res2);
						echo "<option value='".$dett['str_date']."'>".$dett['str_date']."</option>";
					}
				?>
			</select> 
			<br /><br /> 或選擇區間
			        開始:<input type="text" name="log_date_start" id="log_date_start" readonly size="20">
                    結束:<input type="text" name="log_date_end" id="log_date_end" size="20" readonly>
            <br><br />
                <input type='checkbox' name='all_per' value='Y'>顯示全體出缺勤
                <br /><br />
			<input type='submit' name='btn_sub' value='送出' <?php echo $btn_sand;?>/>
			</form>
		</div>
		<?php
		  if(isset($_REQUEST['act']))
          {
                        $start_date = $_REQUEST['log_date_start'];
                        $end_date = $_REQUEST['log_date_end'];
                        if(isset($_REQUEST['all_per']))
                        {
                            $all_per = $_REQUEST['all_per'];
                        }else{
                            $all_per='N';
                        }
                        $pid = $_REQUEST['pid'];
                        $dept = $_REQUEST['dept'];
                        $classtype = $_REQUEST['classtype'];
                        $month = $_REQUEST['month'];
                        //echo $month;
		?>
		<div class="col-md-12 column">
			<form name='tranexcel' action='trans_excel.php'>
            <input type='hidden' name='pid' value='<?php echo $pid;?>'>
            <input type='hidden' name='month' value='<?php echo $month;?>'>
            <input type='hidden' name='dept' value='<?php echo $dept;?>'>
            <input type='hidden' name='classtype' value='<?php echo $classtype;?>'>
            <input type='hidden' name='all_per' value='<?php echo $all_per;?>'>
            <input type='hidden' name='start_date' value='<?php echo $start_date;?>'>
            <input type='hidden' name='end_date' value='<?php echo $end_date;?>'>
        <button type="submit" class="btn btn-info">轉成Excel</button>
		</div>
		<?php
		  }
		?>
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table">
				<thead>
					<tr>
						<th>
							#
						</th>
						<th>
							姓名
						</th>
						<th>
							班別
						</th>
						<th>
							人員卡號
						</th>
						<th>
							上班日期
						</th>
						<th>
							上班時間
						</th>
						<th>
							下班時間
						</th>
						<th>
						    總工時
						</th>
						<th>
							狀態顯示
						</th>
						<th>
							休假狀況
						</th>
						<th>
							誤餐費
						</th>
						<th>
							輪值津貼
						</th>
						<th>
							車資
						</th>
						<th>
							加班狀況
						</th>
						<th>
							備註
						</th>
						<th>
							
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(isset($_REQUEST['act']))
					{
						//$pid= $_REQUEST['Pid'];
						//$month = $_REQUEST['month'];
						$sql = "select a.idx,persionName,classtype,Hams_classname,Hams_Personid,Hams_CardNo,Hams_date,Hams_start,Hams_end,Hams_show,Hams_dinner,Hams_allowance,Hams_memo,Hams_holiday,a.Hams_Car,Hams_overtime,hams_twodays from hams_time a join hams_persion b on a.Hams_CardNo = b.eventCard join hams_classtype c on b.classtype=c.idx where b.disabled=0";
                        $arr_sql = array();
                        if($all_per!='Y')
                        {
                            
                            if($dept) //部門
                            {
                                $arr_sql[] =" b.dept = '$dept' ";
                            }
                            
                            if($pid) //人員
                            {
                                $arr_sql[] =" (a.Hams_CardNo = '$pid' or persionName='$pid')";
                            }
                            if($classtype) //部門
                            {
                                $arr_sql[] =" b.classtype = '$classtype'";
                            }
                            if(count($arr_sql)>0)
                            {
                                $sql.=" and (";
                                $sql.= implode(' or ', $arr_sql);
                                $sql.=")"; 
                            }
                            
                        }
                        
                        if($month)
                        {
                            $sql .=" and substring(Hams_date,1,7)='$month'";
                        }else{
                            if($start_date && $end_date) //時間
                            {
                                $sql .=" and (Hams_date between '$start_date' and '$end_date') ";
                            }    
                        }
                        
                        
						//echo $sql;

						$result= mysql_query($sql);
						$nums = mysql_num_rows($result);
						for($ii=0;$ii<$nums;$ii++)
						{
							mysql_data_seek($result, $ii);
							$detail = mysql_fetch_array($result);
							$idx = $detail['idx'];
							if(date('w',strtotime($detail['Hams_date']))==0 || date('w',strtotime($detail['Hams_date']))==6)
							{
								if($detail['hams_twodays']==1)
								{
									$color= 'class="warning"';
								}
								else {
									$color= '';
								}
							}
							else
							{
								$color= '';
							}
					?>
					<tr <?php echo $color?>>
						<td>
							<?php echo $ii?>
						</td>
						<td>
							<?php echo $detail['persionName'];?>
						</td>
						<td>
							<?php echo $detail['Hams_classname'];?>
						</td>
						<td>
							<?php echo $detail['Hams_CardNo'];?>
						</td>
						<td>
							<?php echo $detail['Hams_date'];?>
						</td>
						<td>
							<?php
							if($detail['Hams_start']!='週休二日' && $detail['Hams_start']!='異常')
							{
								echo substr($detail['Hams_start'],5,11);
							} else {
								echo $detail['Hams_start'];
							}
								
							?>
						</td>
						<td>
							<?php 
							if($detail['Hams_end']=='N')
							{
								if(date('w',strtotime($detail['Hams_date']))==0 || date('w',strtotime($detail['Hams_date']))==6)
								{
									if($detail['hams_twodays']==1)
									{
										echo "週休二日";	
									}
									else 
									{
										echo '異常';
									}
									
								}
								else
								{
										echo '異常';	
								}
							}
							else
							{
								if($detail['Hams_end']!='N')
								{
									echo substr($detail['Hams_end'],5,11);
								} else {
									echo $detail['Hams_end'];
								}
									
							}
							
							
							?>
						</td>
						<td>
						    <?php
						      $timestart = strtotime($detail['Hams_start']);
                              $timeend = strtotime($detail['Hams_end']);
                              //echo $timestart."<BR>";
                              //echo $timeend."<BR>";
                              if($timestart!=0 && $timeend!=0)
                              {
                                  $seconds = $timeend-$timestart;
                                  $workhour = round(($seconds/60)/60);
                                  echo $workhour; 
                              }else{
                                  echo 0;
                              }
						    ?>
						</td>
						<td>
							<?php echo $detail['Hams_show'];?>
						</td>
						<td>
							<?php echo $detail['Hams_holiday'];?>
						</td>
						<td>
							<?php echo $detail['Hams_dinner'];?>
						</td>
						<td>
							<?php echo $detail['Hams_allowance'];?>
						</td>
						<td>
							<?php echo $detail['Hams_Car'];?>
						</td>
						<td>
							<?php echo $detail['Hams_overtime']?>
						</td>
						<td>
							<?php echo $detail['Hams_memo'];?>
						</td>
						<td>
							<button type="button" class="btn btn-info" onclick="modify('<?php echo $detail['Hams_CardNo']?>','<?php echo $idx?>','<?php echo $month?>')">修改</button>
						</td>
					</tr>
					<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>
