<!DOCTYPE html>
<?php
include_once 'config/dbconfig.php';

if(isset($_REQUEST['chk_all']))
{
	$all_checked='checked';
}
else {
	$all_checked='';
}
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>主頁面</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	
	<script language="JavaScript">
		function modify(cid)
		{
			t_url ='classtypemod.php?cid='+cid
			location.href=t_url;
		}
		function del_rec(idx,dis)
		{
			var act = 'class';
			$.ajax({
				url:'ajax_mod.php',
				typr:'POST',
				data:'&act='+act+'&idx='+idx+'&dis='+dis,
				success:function(response)
				{
					if(response==0)
					{
						alert('更新失敗');
					}
					else
					{
						alert('更新成功');	
						if(dis==0)
						{
							window.location.href='classtype.php?chk_all=1'
						}
						else
						{
							window.location.reload();
						}
						
					}
				}
			})
			
		}
		function del_fer(idx)
		{
			var act = 'class_ever';
			if(confirm('確定要永久刪除嗎？'))
			{
				$.ajax({
					url:'ajax_mod.php',
					typr:'POST',
					data:'&act='+act+'&idx='+idx,
					success:function(response)
					{
							if(response==0)
							{
								alert('更新失敗');
							}
							else
							{
								alert('更新成功');	
								window.location.reload();
							}
							
					}
				})
			}
		}
		function del_all(tab)
		{
			var act = 'del_all';
			if(confirm('確定要全部刪除嗎？'))
			{
				$.ajax({
					url:'ajax_mod.php',
					typr:'POST',
					data:'act='+act+'&tab='+tab,
					success:function(response)
					{
						if(response==0)
						{
							alert('更新失敗');
						}
						else
						{
							alert('更新成功');	
							window.location.reload();
							
						}
					}
				})
			}
		}
		
		function rep_all(tab)
		{
			var act = 'rep_all';
			if(confirm('確定要全部復原嗎？'))
			{
				$.ajax({
					url:'ajax_mod.php',
					typr:'POST',
					data:'act='+act+'&tab='+tab,
					success:function(response)
					{
						if(response==0)
						{
							alert('更新失敗');
						}
						else
						{
							alert('更新成功');	
							window.location.reload();
							
						}
					}
				})
			}
		}
	</script>
</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="nav nav-tabs">
				<li>
					<a href="index.php">main</a>
				</li>
				<li>
					<a href="config.php">設定頁</a>
				</li>
				<li class="active">
					<a href="#">班表設定</a>
				</li>
				<li>
					<a href="manclass.php">人員班表設定</a>
				</li>
				<li>
					<a href="deptlist.php">部門別管理</a>
				</li>
				<li>
					<a href="hams_statistics.php">津貼費用計算</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">			
			<button type="button" class="btn btn-info" onclick="location.href='classtypenew.php'">新增班別</button>
			<form name='class' method="post" action="classtype.php">
				<input type='checkbox' name='chk_all' value='1' <?php echo $all_checked?>>顯示全部<br>
				<input type='submit' name='btn_sub' value='送出'>
			</form>
			<div align="right">
				<button type="button" class="btn btn-info" onclick="del_all('Hams_classtype')">全部刪除</button>
				<button type="button" class="btn btn-info" onclick="rep_all('Hams_classtype')">全部復原</button>
			</div>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table">
				<thead>
					<tr>
						<th>
							#
						</th>
						<th>
							班表別
						</th>
						<th>
							上下班時間
						</th>
						<th>
							是否有跨日
						</th>
						<th>
							是否週休二日
						</th>
						<th>
							是否有加班補休
						</th>
						<th>
							車資判斷
						</th>
						<th>
							加班判斷
						</th>
						<th>
							是否有輪班津貼
						</th>
						<th>
							輪班津貼
						</th>
						<th>
							
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if(isset($_REQUEST['chk_all']))
						{
							$sql = "select * from hams_classtype";
						}
						else{
							$sql = "select * from hams_classtype where disabled=0";
						}
						
						$ctrs = mysql_query($sql);
						$ctnums = mysql_num_rows($ctrs);
						for($i=0;$i<$ctnums;$i++)
						{
							mysql_data_seek($ctrs, $i);
							$detail = mysql_fetch_array($ctrs);
							if($detail['Hams_classtype']==1)
							{
								$dailycross='有跨日';
							}
							else
							{
								$dailycross='無跨日';
									
							}
							$cid = $detail['idx'];
							
							if($detail['hams_chkallowance']==1)
							{
								$chk_allowance = '有津貼';
							}
							else
							{
								$chk_allowance = '無津貼';
							}
							
							if($detail['hams_twodays']==1)
							{
								$chk_twodays = '有週休二日';
							}
							else
							{
								$chk_twodays = '無週休二日';
							}
							
							if($detail['hams_chkovertime']==1)
							{
								$chk_overtime = '有加班補休';
							}
							else
							{
								$chk_overtime = '無加班補休';
							}
							
							if($detail['hams_car']==1)
							{
								$chk_car = '有';
							}
							else
							{
								$chk_car = '無';
							}
							
							
							if($detail['hams_overtimeshow']==1)
							{
								$chk_overtimeshow = '有顯示';
							}
							else
							{
								$chk_overtimeshow = '無顯示';
							}
							
							
							
							if($detail['disabled']==0)
							{
								$tr_color="";
								$str_del = "<button type='button' class='btn btn-info' onclick=del_rec('$cid','1')>刪除</button>";
								
							}
							else
							{
								$tr_color ="class='danger'";
								$str_del = "<button type='button' class='btn btn-info' onclick=del_rec('$cid','0')>復原</button>";
								$str_del = $str_del ."<button type='button' class='btn btn-info' onclick=del_fer('$cid')>永久刪除</button>";
							}
					?>
					<tr <?php echo $tr_color;?>>
						<td>
							<?php echo $cid;?>
						</td>
						<td>
							<?php echo $detail['Hams_classname']?>
						</td>
						<td>
							<?php echo $detail['hams_classstarttime']?> - <?php echo $detail['hams_classendtime']?>
						</td>
						<td>
							<?php echo $dailycross?>
						</td>
						<td>
							<?php echo $chk_twodays?>
						</td>
						<td>
							<?php echo $chk_overtime?>
						</td>
						<td>
							<?php echo $chk_car?>
						</td>
						<td>
							<?php echo $chk_overtimeshow?>
						</td>
						<td>
							<?php echo $chk_allowance?>
						</td>
						<td>
							<?php echo $detail['hams_allowancepay']?>
						</td>
						<td>
							<button type="button" class="btn btn-info" onclick="modify(<?php echo $cid;?>)">修改</button>
							<?php echo $str_del;?>
						</td>
					</tr>
					<?php
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>
